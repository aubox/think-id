<?php
namespace Id;

use Id;
use Id\Validate;
use Id\Identity;

use think\DB;
use think\db\Query;
use think\facade\Session;

class Login{
	private $ar;
	private $identity = null;

	public $identityClass; // 'app\models\UserForm'
	public $enableAutoLogin = false;
	public $identityCookie = ['name' => 'identity', 'httpOnly' => true];
	public $enableSession = true;
	public $idParam = '__id';
	public $authTimeout;
	public $absoluteAuthTimeout;
	public $authTimeoutParam = '__expire';
	public $absoluteAuthTimeoutParam = '__absoluteExpire';
	public $autoRenewCookie = true;

	public $loginUrl = ['site/login'];
	public $returnUrlParam = '__returnUrl';
	private $_access = [];

	public function login($user, $field, $duration, $renew)//////
	{
		//if($renew)$this->logout();
		$iden = new Identity();
		if(isset($user['password'])){
			$this->ar = Db::name('d_b_user__aaa');
			switch($field){
				case "email":
					$this->ar = $this->ar->where('th_d_b_email__bc', $user["email"]);
					break;
				case "mobile":
					$this->ar = $this->ar->where('th_d_b_mobile__bd', $user["mobile"]);
					break;
				case "username":
				default:
					$this->ar = $this->ar->where('th_d_b_username__ba', $user["username"]);
					break;
			}
			$this->identity = $this->ar->find();

			if($this->identity){
				if (Validate::validate($user['password'], $this->identity["th_d_b_password__bb"])) {
					Id::$user = (object)$this->identity;
					$iden->setIdentity($this->idParam, $this->identity[$this->ar->getPk()]);
				} else {
					$this->identity = null;
				}
			}
		}else{
			if(Session::has($this->idParam)){
				$this->ar = Db::name('d_b_user__aaa');
				$this->ar = $this->ar->where($this->ar->getPk(), Session::get($this->idParam));
				$this->identity = $this->ar->find();

				if($this->identity){
					Id::$user = (object)$this->identity;
					$iden->setIdentity($this->idParam, $this->identity[$this->ar->getPk()]);
				}
			}
		}

		if($this->identity){
			return $this->setStatus(true);
		} else {
			return $this->setStatus(false);
		}
	}

	public function logout($destroySession = true)
	{
		Session::clear();
	}

	public function setStatus($state)
	{
		Id::$isGuest = !Id::$isUser = $state;
		return $state;
	}
}