<?php
use Id\Login;
use Id\Validate;

use think\db\Query;

class Id{
	private function __construct(){}
	private static $instance;
	public static function getInstance()
	{
		if (is_null(static::$instance)) {
			static::$instance = new static;
		}
		return static::$instance;
	}
	public static function setInstance($instance)
	{
		static::$instance = $instance;
	}

	public static $user;
	public static $isUser = false;
	public static $isGuest = true;

	public static function status()
	{
		if(static::$isUser){
			return true;
		}else{
			return false;
		}
	}

	public static function login($user=[], $field="username", $duration=0, $renew=false)
	{
		$_this = static::$instance;
		$login = new Login();
		return $login->login($user, $field, $duration, $renew);
	}

	public static function logout($user=[], $field="username", $duration=0, $renew=false)
	{
		$_this = static::$instance;
		$login = new Login();
		return $login->logout();
	}
}