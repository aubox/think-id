<?php
use think\Db;
use think\facade\Cookie;
use think\facade\Session;

if (!function_exists('idGetName')) {
	function idGetName()
	{
		if(Id::$isUser){
			return Id::$user->th_d_b_username__ba;
		}else{
			return "No Login";
		}
	}
}
